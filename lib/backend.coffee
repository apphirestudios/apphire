require './globals'
koa = require('koa')()
http = require 'http'
https = require 'https'
bodyParser = require 'koa-body'
json = require 'koa-json'
koaRouter = require 'koa-router'
path = require 'path'
fs = require 'fs'
SocketServer = require './socket-server'
Emitter = require './emitter'
Logger = require './logger'
nodeModule = require 'module'
Request = require './request'

server = http.createServer(koa.callback())
socket = new SocketServer(server)

module.exports = class Apphire extends Emitter
  request: new Request()
  isServer: true
  isClient: false
  use: koa.use.bind koa
  callback: koa.callback.bind koa
  server: server
  socket: socket
  router: koaRouter()
  logger: new Logger() #Override this to any provider you like
  notify: (msg)->
    @logger.log msg
  error: (e)->
    @logger.error e

  constructor: ()->
    super()
    @port = process.env.PORT or 5000
    self = @

    global.log = @notify.bind @

    @on "backend:starting", =>
      @notify 'Application starting...'
    @on 'http:listening', =>
      @notify "HTTP server listening on http://localhost:#{@port}"

    @use json()
    @use bodyParser
      formidable:
        uploadDir: './public/uploads'
        keepExtensions: true
      multipart: true,
      urlencoded: true
    @use (next)->
      try
        yield next
      catch e
        self.error e
        @status = e.status or 500
        @body = {message: e.message}
        if process.env.NODE_ENV is "development" then @body.stack = e.stack

    @use(@router.routes())
    @use(@router.allowedMethods())
    #We provide this service so that APPHIRE-STUDIO can determine which files to watch
    if process.env.NODE_ENV is 'development'
      @router.get '/__required__', (next)->
        @body = (p for p of nodeModule._cache)
        yield next

  start: ()-> async =>
    @emit "backend:starting"
    try
      yield @_listen()
    catch e
      @error e
      throw e

  _listen: ()-> async =>
    yield new Promise (resolve, reject)=>
      @server.listen @port
      @server.on 'error', (e)=>
        @emit "http:error", e
        reject e
      @server.on 'listening', (server)=>
        @emit "http:listening", server
        resolve server





