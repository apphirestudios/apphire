WebSocketServer = require('ws').Server #this lib is the fastest
async = require 'co'
Emitter = require './emitter'

class SocketConnection extends Emitter
  constructor: (@_wsConn, @_server)->
    super()
    @_wsConn.on 'message', (_wsMsg)=> async =>
      m = JSON.parse _wsMsg
      if m._requestId
        try
          response = yield async => @_server._requestHandlers[m.fName](m.fArg)
        catch e
          response = {error: e.stack}       
        
        @_wsConn.send JSON.stringify response =
          _requestId: m._requestId
          response: response
      else
        @emit m.event, m.data
  
  send: (event, data)->
    message =
      event: event
      data: data
    @_wsConn.send JSON.stringify(message)

module.exports = class SocketServer extends Emitter
  _requestHandlers: {}
  constructor: (httpServer)->
    super()
    @_wsServer = new WebSocketServer({ server: httpServer })
    @_wsServer.on 'connection', (_wsConn)=> @emit 'connection', new SocketConnection(_wsConn, @)

  broadcast: (event, data)->
    message =
      event: event
      data: data
    for _wsConn in @_wsServer.clients
      _wsConn.send JSON.stringify(message)

  response: (fName, fn)->
    @_requestHandlers[fName] = fn
