reqwest = require 'reqwest'
async = require 'co'
EventEmitter2 = require('eventemitter2').EventEmitter2
ISO_DATE = /^(\d{4})-(\d{2})-(\d{2})T(\d{2}):(\d{2}):(\d{2}(?:\.\d*))(?:Z|(\+|-)([\d|:]*))?$/
isPlainObject = require 'is-plain-object'
extend = (obj, props) ->
  for prop of props
    if props.hasOwnProperty(prop)
      obj[prop] = props[prop]

parseDates = (data)->
  for k,v of data
    if ISO_DATE.test(v)
      data[k] = new Date(Date.parse(v))
    if isPlainObject(v)
      parseDates(v)

class Request extends EventEmitter2
  base: ''
  constructor: (@config)->
    super()
  request: (config)-> async =>
    try
      if @config then extend config, @config()
      config.type = 'json'
      config.contentType = 'application/json; charset=UTF-8'
      config.data = JSON.stringify config.data
      request = reqwest config
      @emit('start', request)
      response = yield request
      @emit('success', response)
      if response then parseDates response
      return response
    catch e
      errorMessage = e.response?.data?.message or e.response?.data or e.response or e
      try
        errorMessage = JSON.parse errorMessage
      catch e

      err = new Error(errorMessage.message or errorMessage, errorMessage)
      @emit('error', err)
      throw new Error(err)
    finally
      @emit('end', request)

  get: (url)-> async =>
    return yield @request
      method: 'get'
      url: @base + url

  post: (url, data)-> async =>
    return yield @request
      method: 'post'
      url: @base + url
      data: data

  put: (url, data)-> async =>
    return yield @request
      method: 'put'
      url: @base + url
      data: data

  patch: (url, data)-> async =>
    return yield @request
      method: 'patch'
      url: @base + url
      data: data

  delete: (url, data)-> async =>
    return yield @request
      method: 'delete'
      url: @base + url
      data: data

module.exports = Request