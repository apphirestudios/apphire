W3CWebSocket = require('websocket').w3cwebsocket
async = require 'co'
Emitter = require './emitter'
guid = ->
  s4 = ->
    Math.floor((1 + Math.random()) * 0x10000).toString(16).substring 1
  return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4()

class SocketConnection extends Emitter
  constructor: (@w3cSocket)->
    super()
    @w3cSocket.onmessage = (message)=>
      if typeof message.data is 'string'
        @emit 'message', JSON.parse message.data
  send: (data)->
    @w3cSocket.send JSON.stringify(data)

_WSClient =
  connect: (adress)-> async ->
    return connection = yield new Promise (resolve, reject)=>
      try
        w3cSocket = new W3CWebSocket(adress, 'echo-protocol')
        w3cSocket.onerror = (e)-> reject e
        w3cSocket.onopen = ->
          resolve new SocketConnection @
        #client.on 'connectFailed', reject
        #client.connect adress, 'echo-protocol'
      catch e
        reject e

class ReqResEmitter extends Emitter
  constructor: -> super()

module.exports = class SocketClient extends Emitter
  @_WSClient: _WSClient
  reqResEmitter: new ReqResEmitter()
  constructor: ()-> super()

  connect: (adress)-> async =>
    @connection = yield SocketClient._WSClient.connect(adress)
    @connection.on 'message', (message)=>
      if message._requestId
        @reqResEmitter.emit message._requestId, message.response
      else
        @emit message.event, message.data

  send: (event, data)->
    message =
      event: event
      data: data
    @connection.send message

  request: (fName, fArg)-> async =>
    yield new Promise (resolve, reject)=>
      _requestId = guid()
      message =
        _requestId: _requestId
        fName: fName
        fArg: fArg
      @connection.send message
      @reqResEmitter.once _requestId, (response)->
        if response?.error?
          reject new Error response.error
        else
          resolve response