EventEmitter2 = require('eventemitter2').EventEmitter2
module.exports = class Emitter extends EventEmitter2
  constructor: -> super({wildcard: true, delimiter: ':'})