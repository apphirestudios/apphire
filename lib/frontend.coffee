async = require 'co'
Emitter = require './emitter'
SocketClient = require "./socket-client"
Request = require './request'

module.exports = class Apphire extends Emitter
  request: new Request()
  socket: new SocketClient()

  constructor: ->
    if not @apiEndpoint
      if not (window.location?.hostname?)
        throw new Error "Apphire frontend core is launched not in browser environment. This means you must explicitly set apiEndpoint as apphire instance property."
      else
        @apiEndpoint = "#{window.location.protocol}//#{window.location.hostname}:#{window.location.port or '80'}"
    @request.base = @apiEndpoint
    super()


  socketConnect: -> async =>
    wsAdress = @socketEndpoint or @apiEndpoint.replace("http://", "ws://")
    yield @socket.connect(wsAdress)

  launch: -> async =>
    yield @_waitForDOMReady()

  _waitForDOMReady: -> async =>
    yield new Promise (resolve, reject)=>
      checkState = =>
        if document.readyState is 'complete'
          return resolve(@)
      checkState()
      document.addEventListener 'DOMContentLoaded', => resolve @

  isClient: true
  isServer: false

