WebSocketClient = require('websocket').client
async = require 'co'
Emitter = require '../lib/emitter'

class WebSocketConnection extends Emitter
  constructor: (@_wsConnection)->
    super()
    @_wsConnection.on 'message', (message)=>
      if message.type is 'utf8'
        parsed = JSON.parse message.utf8Data
        @emit 'message', parsed
  send: (data)->
    @_wsConnection.sendUTF JSON.stringify(data)

module.exports = _WSClient =
  connect: (adress)-> async ->
    return connection = yield new Promise (resolve, reject)=>
      try
        client = new WebSocketClient()
        client.on 'connect', (_wsConnection)=> 
          resolve new WebSocketConnection _wsConnection

        client.on 'connectFailed', reject
        client.connect adress, 'echo-protocol'
      catch e
        reject e



