fs = require 'fs'
fork = require('child_process').fork
process.execArgv.pop()
#remove ['--debug'] from arguments list for avoiding EADDRINUSE
#when trying to fork processes

SocketClientNode = require './socket-client-node'
SocketClient = require '../lib/socket-client'
SocketClient._WSClient = SocketClientNode


PORT = 3000
NODE_ENV = "development"

describe "apphire-core", ->
  apphireFrontend = new (require '../lib/frontend')
  apphireFrontend._getSocketAdress = ->
    return "ws://localhost:#{PORT}/"

  backendAdress = "http://localhost:#{PORT}"

  forkedBackend = undefined

  spawnBackend = ->
    forkedBackend = fork './bin/backend', [],
      cwd: './test-app/'
      env:
        PORT: PORT
        NODE_ENV: NODE_ENV
      silent: true
    yield new Promise (resolve, reject)->
      forkedBackend.stdout.on 'data', (message)->
        #log ''  + message
        #UNCOMMENT FOR DEBUGGING
        if message.indexOf('HTTP server listening on') is 0 then resolve()
      forkedBackend.stderr.on 'data', (e)->
        log e.toString()
        reject new Error e

  killBackend = ->
    forkedBackend.kill()

  before spawnBackend
  after killBackend
  describe "core", ->
    it "Sets client and server properties of backend", ->
      apphireBackend = require '../lib/backend'
      (new apphireBackend()).isServer.should.equal true
      (new apphireBackend()).isClient.should.equal false

    it "returns 404", ->
      yield request(backendAdress).get("/not-existent-route").expect("Not Found").end()

    it "returns index page", ->
      yield request(backendAdress).get("/").expect("OK-INDEX").end()

    it "returns static asset", ->
      yield request(backendAdress).get("/test.css").expect("OK-CSS").end()

    it "echoes body (simple test of routing system, we're not going to re-test koa", ->
      yield request(backendAdress).post("/test/echo").send({ok: "go"}).expect(200, {ok: "go"}).end()

    it "returns full error stack in dev mode", ->
      yield request(backendAdress).get("/test/throw").expect(500, {message: 'ERR_MESSAGE', stack: 'ERR_STACK'}).end()

    it "no stacktraces leaked to user in prod mode", ->
      yield async killBackend
      NODE_ENV = 'production'
      yield async spawnBackend
      yield request(backendAdress).get("/test/throw").expect(500, {message: 'ERR_MESSAGE'}).end()
      yield async killBackend
      NODE_ENV = 'development'
      yield async spawnBackend

    it "Sets client and server properties of frontend", ->
      apphireFrontend.isServer.should.equal false
      apphireFrontend.isClient.should.equal true

    it "launches frontend", ->
      global.document =
        readyState: 'not complete'
        addEventListener: (name, fn)->
          if name is 'DOMContentLoaded' then setTimeout fn, 100
      app = yield apphireFrontend.launch()
      app.should.equal apphireFrontend

  describe "http", ->
    it "request 404", ->
      yield throws apphireFrontend.request.get(backendAdress + "/not-existent-route"), /Not Found/

    it "echo", ->
      resp = yield apphireFrontend.request.post(backendAdress + "/test/echo", { ok: 'go' })
      resp.ok.should.equal 'go'

    it "emits start", ->
      yield new Promise (resolve, reject)-> async ->
        apphireFrontend.request.once 'start', ->
          resolve()
        yield apphireFrontend.request.get(backendAdress + "/")

    it "emits success", ->
      yield new Promise (resolve, reject)-> async ->
        apphireFrontend.request.once 'success', ->
          resolve()
        yield apphireFrontend.request.post(backendAdress + "/test/echo", { ok: 'go' })

    it "emits error", ->
      yield new Promise (resolve, reject)-> async ->
        apphireFrontend.request.once 'error', ->
          resolve()
        yield throws apphireFrontend.request.get(backendAdress + "/not-existent-route"), /Not Found/

  describe "socket", ->
    before ->
      yield apphireFrontend.launch()

    it "broadcasting", ->
      yield new Promise (resolve)-> async ->
        apphireFrontend.socket.on 'broadcast', (message)->
          if message.test is 'OK' then resolve()
        yield request(backendAdress).get("/test-socket-broadcast")

    it "echo", ->
      yield new Promise (resolve)-> async ->
        apphireFrontend.socket.on 'echo', (message)->
          if message.test is 'ECHO' then resolve() else reject(new Error('Response is not match'))
        apphireFrontend.socket.send('echo', {test: 'ECHO'})

    it "req-res: undefined", ->
      response = yield apphireFrontend.socket.request 'undefined'
      expect(response).to.be.undefined

    it "req-res: echo", ->
      echo = {ok: 'ECHO'}
      response = yield apphireFrontend.socket.request 'echo', echo
      response.should.deep.equal echo

    it "req-res: throw", ->
      yield apphireFrontend.launch()
      yield throws apphireFrontend.socket.request 'throw', /ERR/



