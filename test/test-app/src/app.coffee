Apphire = require '../../../lib/backend'

apphire = new Apphire(process.env)
path = require 'path'

staticServer = require 'koa-static'

apphire = new Apphire(process.env)
apphire.use staticServer(path.join(__dirname, '../public'))

apphire.socket.on 'connection', (conn)->
  conn.on 'echo', (msg)->
    conn.send 'echo', msg

apphire.router.post "/test/echo", (next)->     
  @body = @request.body
  
apphire.router.get "/test/throw", (next)-> 
  e = new Error()
  e.message = 'ERR_MESSAGE'
  e.stack = 'ERR_STACK'
  throw e
  yield next

apphire.router.get '/test-socket-broadcast', (next)->
  apphire.socket.broadcast 'broadcast', {test: 'OK'}
  yield next

apphire.start().catch (e)->
  console.error e.stack or e
  setTimeout (-> process.exit(1)), 500 

apphire.socket.response 'undefined', (msg)->
  return undefined

apphire.socket.response 'echo', (msg)->
  yield new Promise (r)-> setTimeout r, 10
  return msg

apphire.socket.response 'throw', (msg)->
  throw new Error 'ERR'
  return msg