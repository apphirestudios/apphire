"use strict";
if (typeof window !== "undefined" && window !== null) {
	module.exports = require('./lib/frontend');
} else {
	eval("require('coffee-script').register();")
	module.exports = eval('require("./lib/backend")');
	//We use eval to make this require call 'invivible' to
	//apphire-bundler, so when it parses this file as
	//client-side module, it bundles only frontend part
}
