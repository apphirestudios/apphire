Apphire = require '../lib/backend'
apphire = new Apphire(process.env)
apphire.start().catch (e)->
  console.error e.stack or e
  setTimeout (-> process.exit(1)), 500  